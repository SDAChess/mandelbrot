# mandelbrot

Mandelbrot fractal's visualizer with pygame library. 

![example1](/assets/example1.png)

## Params
In main.py you can change parameters for instance "color" (in RGB) for fractal color, "size" for window size.
```py
PARAMS_ = {"color": (0,0,0), "size": (600,600), "coords": (-1.5, 2, -1, 2)}
```